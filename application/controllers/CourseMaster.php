<?php

/**
 * Created by PhpStorm.
 * User: SakaHeroji
 * Date: 8/17/15
 * Time: 10:17 PM
 */
class CourseMaster extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('frameconstructor');
    }
    public function dashboard()
    {
        $data = $this->frameconstructor->constructAll();
        $this->load->view('/layout/framelogin',$data);
    }
}