<?php

class Product extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('frameconstructor');
		$this->constructPages();
        $this->load->database();
	}

	  public function constructPages()
    {
        $this->viewdata['menu'] = $this->frameconstructor->createSidebarMenu();
        $this->viewdata['profile'] = $this->frameconstructor->createProfile();
        $this->viewdata['righttopmenu'] = $this->frameconstructor->createRightTopMenu();

    }


    public function viewProduct()
    {
        $query = $this->db->query('select * from product');
        $this->viewdata['data'] = $query->result();

        $query2 = $this->db->query('select * from product where nama_product like "%navigator%"');
        $this->viewdata['data2'] = $query2->result();

		$this->viewdata['pages'] ='/pages/Product/viewProduct';
        $this->load->view('/layout/framelogin',$this->viewdata);

    }

    public function searchProduct($cari)
    {
        $query = $this->db->query('select * from product where nama_product like "%'.$cari.'%"');
        $this->viewdata['data'] = $query->result();


        $this->viewdata['pages'] ='/pages/Product/viewProduct';
        $this->load->view('/layout/framelogin',$this->viewdata);
    }

    public function ajaxGetProductByID($id)
    {
        $product = $this->db->query('select * from product where id_product="'.$id.'"');
        $result = $product->result();
        $result['json'] = json_encode($result);
        echo json_encode($result);
               
    }

    public function addProduct()
    {
        $product = $this->db->query('select * from product');
        $this->viewdata['data'] = $product->result();

        $brand = $this->db->query('select * FROM brand order by brand');
        $this->viewdata['brand'] = $brand->result();

        $kategori = $this->db->query('select * FROM kategori order by kategori');
        $this->viewdata['kategori'] = $kategori->result();

    	$this->viewdata['pages'] ='/pages/Product/addProduct';
        $this->load->view('/layout/framelogin',$this->viewdata);
    }

    public function insertProduct()
    {
        //$this->db->insert into('product_name','description');
        //$this->db->from('product');
        //$this->db->like('product_name','ASUS');
        //$this->db->where('product_name like ',)
    


        $data = array(
            #'in_date'       => $this->input->post('inDate'),
            #'image'         => $this->input->post('image'),
            'id_brand'      => $this->input->post('brand'),
            'nama_product'  => $this->input->post('namaBarang'),            
            'deskripsi'     => $this->input->post('deskripsi'),
            'id_kategori'   => $this->input->post('kategori'),
            'size'          => $this->input->post('size'),
            'warna'         => $this->input->post('warna'),
            'qty'           => $this->input->post('qty'),
            'hargaM'        => $this->input->post('hargaM'),
            'hargaJ'        => $this->input->post('hargaJ'),
            );

        $this->db->insert('product',$data);
        redirect('Product/viewProduct');


    }   

    public function editProduct($idProduk)
    {
        
        //$queryEdit = $this->db->query('select * from product where product_id = 3');
        //$this->viewdata['dataId'] = $queryEdit->result();
/*
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id',$idProduk);
        $query = $this->db->get();
        return $dataId = $query->result();
*/
        $brand = $this->db->query('select * FROM brand order by brand');
        $this->viewdata['brand'] = $brand->result();

        $kategori = $this->db->query('select * FROM kategori order by kategori');
        $this->viewdata['kategori'] = $kategori->result();

        $query = $this->db->query('select * from product where id_product='.$idProduk);
        $this->viewdata['dataId'] = $query->result();


        $this->viewdata['pages'] ='/pages/Product/editProduct';
        $this->load->view('/layout/framelogin',$this->viewdata);

       
        


    }

    public function saveProduct($idProduk)
    {
         $data = array(
            
            'id_brand'      => $this->input->post('brand'),
            'nama_product'  => $this->input->post('namaBarang'),            
            'deskripsi'     => $this->input->post('deskripsi'),
            'id_kategori'   => $this->input->post('kategori'),
            'size'          => $this->input->post('size'),
            'warna'         => $this->input->post('warna'),
            'qty'           => $this->input->post('qty'),
            'hargaM'        => $this->input->post('hargaM'),
            'hargaJ'        => $this->input->post('hargaJ'),
            #'in_date'
           
            );

        $this->db->where('id_product',$idProduk);
        $this->db->update('product',$data);
        redirect('Product/viewProduct');

        #echo "Ini function SAVE";
    }

    public function deleteProduct($idProduk)
    {
        $query = $this->db->query('delete from product where id_product='.$idProduk);

        //$this->load->view('/layout/framelogin',$this->viewdata);
        echo "Success Delete Product ID";
        redirect('Product/viewProduct');
    }


}

?>