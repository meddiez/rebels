<?php

/**
 * Created by PhpStorm.
 * User: SakaHeroji
 * Date: 8/21/15
 * Time: 11:26 AM
 */
class Home extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->load->library('frameconstructor');
        $this->constructPages();
    }

    public function constructPages()
    {
        $this->viewdata['menu'] = $this->frameconstructor->createSidebarMenu();
        $this->viewdata['profile'] = $this->frameconstructor->createProfile();
        $this->viewdata['righttopmenu'] = $this->frameconstructor->createRightTopMenu();

    }

    public function dashboard()
    {
 	    $this->viewdata['pages'] ='/pages/Home/dashboard';
        $this->load->view('/layout/framelogin',$this->viewdata);

    }

    public function login(){

        $this->load->view('/pages/home/login');
    }

    /*public function signup()
    {
        $this->load->view('/pages/home/signup');
    }*/
}