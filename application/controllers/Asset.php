<?php

class Asset extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('frameconstructor');
		$this->constructPages();
        $this->load->database();
	}

	  public function constructPages()
    {
        $this->viewdata['menu'] = $this->frameconstructor->createSidebarMenu();
        $this->viewdata['profile'] = $this->frameconstructor->createProfile();
        $this->viewdata['righttopmenu'] = $this->frameconstructor->createRightTopMenu();

    }


    public function stockOpname()
    {
        #$query = $this->db->query('select id_product, nama_product, qty from product');
        #$this->viewdata['so'] = $query->result();

        $this->viewdata['pages'] ='/pages/Asset/stockOpname';
        $this->load->view('/layout/framelogin',$this->viewdata);
    }

     public function stockOpnameDetail()
    {
        #$query = $this->db->query('select id_product, nama_product, qty from product');
        #$this->viewdata['so'] = $query->result();

        $query = $this->db->query('select  pr.id_product, nama_product, qty_system
        , qty_check,note  from detail_stok_opname op join product pr on op.id_product = pr.id_product');
        $this->viewdata['data'] = $query->result();

        $this->viewdata['pages'] ='/pages/Asset/stockOpnameDetail';
        $this->load->view('/layout/framelogin',$this->viewdata);


        
    }

    public function profit()
    {
        $this->viewdata['pages'] ='/pages/Asset/profit';
        $brand = $this->db->query('select * FROM brand order by brand');
        $this->viewdata['brand'] = $brand->result();

        $kategori = $this->db->query('select * FROM kategori order by kategori');
        $this->viewdata['kategori'] = $kategori->result();

        $this->load->view('/layout/framelogin',$this->viewdata);
    }   


    public function assetValue()
    {
        
        #$this->db->select()
    } 

   

}

?>