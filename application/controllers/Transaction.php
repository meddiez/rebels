<?php

class Transaction extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('frameconstructor');
		$this->constructPages();
        $this->load->database();
	}

	  public function constructPages()
    {
        $this->viewdata['menu'] = $this->frameconstructor->createSidebarMenu();
        $this->viewdata['profile'] = $this->frameconstructor->createProfile();
        $this->viewdata['righttopmenu'] = $this->frameconstructor->createRightTopMenu();

    }

    public function soldProduct()
    {
        $query = $this->db->query('select * from sold');
        $this->viewdata['data'] = $query->result();

        $this->viewdata['pages'] ='/pages/Transaction/soldProduct';
        $this->load->view('/layout/framelogin',$this->viewdata);
    }

    public function addSoldProduct()
    {
        $brand = $this->db->query('select * FROM brand order by brand');
        $this->viewdata['brand'] = $brand->result();

        $kategori = $this->db->query('select * FROM kategori order by kategori');
        $this->viewdata['kategori'] = $kategori->result();
        
        $this->viewdata['pages'] ='/pages/Transaction/addSoldProduct';
        $this->load->view('/layout/framelogin',$this->viewdata);   
    }

    public function insertSold()
    {
        
    }

    public function soldConfirm()
    {
        $this->viewdata['pages'] ='/pages/Transaction/soldConfirm';
        $this->load->view('/layout/framelogin',$this->viewdata);
    }


    public function draft()
    {
    	$this->viewdata['pages'] ='/pages/Transaction/draft';
        $this->load->view('/layout/framelogin',$this->viewdata);
    }

   

}

?>