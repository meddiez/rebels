<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: SakaHeroji
 * Date: 8/18/15
 * Time: 6:09 PM
 */
class Frameconstructor
{

    public function createProfile()
    {
        $profile=" <div class='profile'>
                    <a href=''#'>
    <img src='".base_url()."images/people/110/guy-5.jpg' alt='people' class='img-circle width-80' />
                    </a>
                    <h4 class='text-display-1 margin-none'>Staff</h4>
                </div>";
        return $profile;
    }



    public  function createSidebarMenu()
    {

            $menu = "
           
                <li><a href='".base_url()."Home/dashboard'><span>Home</span></a></li>      
                <li><a href='".base_url()."Product/viewProduct'><span>Product</span></a></li>
                <li><a href='".base_url()."Transaction/soldProduct'><span>Sales</span></a></li>
                <li><a href='".base_url()."Transaction/draft'><span>Draft</span></a></li>
                <!--<li><a href='".base_url()."Asset/profit'><span>Profit</span></a></li>-->
                <li><a href='".base_url()."Asset/stockOpname'><span>Stock Opname</span></a></li>
            
                ";

        return $menu;

    }

    public function constructAll()
    {
        $data = array('profile'=> $this->createProfile(),
                      'menu'=> $this->createSidebarMenu(),
                      'righttopmenu' => $this->createRightTopMenu()

            );
        return $data;
    }


    public function createRightTopMenu()
    {
        /*$rightmenu = "<a href='#' class='dropdown-toggle user' data-toggle='dropdown'>
            <img src='".base_url()."images/people/110/woman-3.jpg' alt='Bill' class='img-circle' width='40' /> Admin <span class='caret'></span>
                        </a>
                        <ul class='dropdown-menu' role='menu'>
                            
                            <li><a href='#'>Logout</a></li>
                        </ul>"; //<li><a href='#'>Account</a></li>
        return $rightmenu;
        */

        $rightmenu = "
            <a href='".base_url()."Home/login'> <img src='".base_url()."images/people/110/guy-5.jpg' alt='Bill' class='img-circle' width='40' /> Logout  <span class='glyphicon glyphicon-log-out'></span></a>
        ";
        return $rightmenu;
    }

}