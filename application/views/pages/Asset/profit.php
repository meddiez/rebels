<div class="container-fluid">

	<div class="page-section">
		<h1 class="text-display-1 margin-none">PROFIT</h1>
				
	</div>
	<script src="<?php echo base_url(); ?>js/app/app.js"></script>

	<div class="row" data-toggle="isotope">
		<div class="item col-xs-12 col-lg-6">
			<div class="panel panel-default paper-shadow" data-z="0.5">
					 <label for="inDate" class="col-xs-2 control-label">Tanggal Awal : </label>
				        <div class="col-xs-3 date">
				            <div class="input-group date" id="awal">
				                <input type="text" class="form-control" value="<?php echo set_value('awal');?>">
				                <span class="input-group-addon">
				                	<span class="glyphicon glyphicon-calendar"></span>
				                </span>
				                <script type="text/javascript">
						            $(document).ready(function(){
						            	 $('#awal').datepicker({
						                    "dateFormat":"yyyy-mm-dd"
						                });
						            });
						       </script>
				            </div>
				        </div>

				         <label for="inDate" class="col-xs-2 control-label">Tanggal Akhir : </label>
				        <div class="col-xs-3 date">
				            <div class="input-group date" id="akhir">
				                <input type="text" class="form-control" value="<?php echo set_value('akhir');?>">
				                <span class="input-group-addon">
				                	<span class="glyphicon glyphicon-calendar"></span>
				                </span>
				                <script type="text/javascript">
						            $(document).ready(function(){
						            	 $('#akhir').datepicker({
						                    "dateFormat":"yyyy-mm-dd"
						                });
						            });
						       </script>
				            </div>
				        </div>


				        <?php echo form_submit('submit', 'Go!','class="btn btn-primary"'); ?>

				</div>
			</div>
			<div class="item col-xs-12 col-lg-6">
				<div class="table-responsive">
					<table class="table table-hover">
						
							<td class="info">#</td>
							<td class="info">Brand</td>
							<td class="info">Kategori</td>							
							<td class="info">Qty</td>
							<td class="info">Total Modal</td>
							<td class="info">Total Terjual</td>
							<td class="info">Profit</td>

						
						<?php 

							$data = array(
								array(
									"brand"=>"Jack Wolfskin",
									"kategori"=>"Jacket Waterproof",
									"qty" => 6,
									"total modal" => 330000,
									"total terjual" => 600000,
									"profit" => 270000
									),
								array(
									"brand"=>"Jack Wolfskin",
									"kategori"=>"T-Shirt",
									"qty" => 21,
									"total modal" => 1365000,
									"total terjual" => 2520000,
									"profit" => 1155000
									),
							
								array(
									"brand"=>"Millet",
									"kategori"=>"Jaket Inner/Polar",
									"qty" => 8,
									"total modal" => 2000000,
									"total terjual" => 3200000,
									"profit" => 1200000
									),
								array(
									"brand"=>"Millet",
									"kategori"=>"T-Shirt",
									"qty" => 6,
									"total modal" => 330000,
									"total terjual" => 600000,
									"profit" => 270000
									),								
								array(
									"brand"=>"Salomon",
									"kategori"=>"Jacket Waterproof",
									"qty" => 2,
									"total modal" => 700000,
									"total terjual" => 1300000,
									"profit" => 600000
									),	
								array(
								"brand"=>"The North Face",
								"kategori"=>"Celana",
								"qty" => 4,
								"total modal" => 980000,
								"total terjual" => 1400000,
								"profit" => 420000
								),								
								array(
									"brand"=>"The North Face",
									"kategori"=>"Jacket Inner/Polar",
									"qty" => 3,
									"total modal" => 750000,
									"total terjual" => 1100000,
									"profit" => 350000
									),								
								array(
									"brand"=>"The North Face",
									"kategori"=>"Jacket Waterproof",
									"qty" => 2,
									"total modal" => 700000,
									"total terjual" => 1300000,
									"profit" => 600000
									),
								array(
									"brand"=>"Ziener",
									"kategori"=>"Glove",
									"qty" => 2,
									"total modal" => 245000,
									"total terjual" => 1300000,
									"profit" => 600000
									)								

								);
							for($i=0;$i<5;$i++)
							{


						?>
						<tr>
							<td><?php echo $i+1;?></td>
							<td><?php echo $data[$i]["brand"];?></td>
							<td><?php echo $data[$i]["kategori"];?></td>
							<td><?php echo $data[$i]["qty"];?></td>
							<td><?php echo $data[$i]["total modal"];?></td>
							<td><?php echo $data[$i]["total terjual"];?></td>
							<td align="right"><?php echo $data[$i]["profit"];?></td>
							
						</tr>
						
						<?php
							}
						?>
						<tr>
							<td colspan="6" align="right">Total Ongkir :</td>
							<td align="right">335000</td>
						</tr>
						<tr>
							<td colspan="6" align="right">Netto Profit :</td>
							<td align="right">4530000</td>
						</tr>
			</div>
		</div>
	</div>
</div>