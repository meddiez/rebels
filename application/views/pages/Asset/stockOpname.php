<div class="container-fluid">
	<div class="page-section">
		<h1 class="text-display-1 margin-none">Stock Opname</h1>
	</div>

	<div class="row" data-toggle="isotope">
		<div class="panel panel-default paper-shadow" data-z="0.5">
			<div class="table-responsive">
                    <table class="table table-hover col-xs-12">
                        <thead>
                            <tr>
                                <td class="info col-xs-1">ID Barang</td>
                                <td class="info col-xs-4">Nama Barang</td>
                                <td class="info col-xs-1">Q.Syst</td>
                                <td class="info col-xs-1">Qty Check</td>
                                <td class="info col-xs-5">Note</td>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                            	<td align="center">1</td>
                                <td>JWS Paw Print T-Shirt</td>
                                <td>10</td>
                                <td><input type="text" class="form-control" id="qtyCek" name="qtyCek" value="<?php echo set_value('qtyCek');?>"></td>
                                <td><input type="text" class="form-control" id="note" name="note" value="<?php echo set_value('note');?>"></td>
                                                           
                            </tr>

                            <tr>
                                <td align="center">2</td>
                                <td>Millet Quick Dry T-Shirt</td>
                                <td>1</td>
                                <td><input type="text" class="form-control" id="qtyCek" name="qtyCek" value="<?php echo set_value('qtyCek');?>"></td>
                                <td><input type="text" class="form-control" id="note" name="note" value="<?php echo set_value('note');?>"></td>                       
                            </tr>

                            <tr>
                                <td align="center">3</td>
                                <td>Buffalo Base Layer Long Shirt</td>
                                <td>2</td>
                                <td><input type="text" class="form-control" id="qtyCek" name="qtyCek" value="<?php echo set_value('qtyCek');?>"></td>
                                <td><input type="text" class="form-control" id="note" name="note" value="<?php echo set_value('note');?>"></td>
                                                           
                            </tr>
                            <tr>
                                <td align="center">5</td>
                                <td>TNF Dubs Insulated Snow Jacket </td>
                                <td>1</td>
                                <td><input type="text" class="form-control" id="qtyCek" name="qtyCek" value="<?php echo set_value('qtyCek');?>"></td>
                                <td><input type="text" class="form-control" id="note" name="note" value="<?php echo set_value('note');?>"></td>
                                                           
                            </tr>
                            
                            <tr>
                                <td align="center">8</td>
                                <td>Berghaus Navigator Octane</td>
                                <td>1</td>
                                <td><input type="text" class="form-control" id="qtyCek" name="qtyCek" value="<?php echo set_value('qtyCek');?>"></td>
                                <td><input type="text" class="form-control" id="note" name="note" value="<?php echo set_value('note');?>"></td>
                                                           
                            </tr>

                            <tr>
                                <td align="center">9</td>
                                <td>Berghaus Navigator Pant</td>
                                <td>1</td>
                                <td><input type="text" class="form-control" id="qtyCek" name="qtyCek" value="<?php echo set_value('qtyCek');?>"></td>
                                <td><input type="text" class="form-control" id="note" name="note" value="<?php echo set_value('note');?>"></td>                       
                            </tr>

                            <tr>
                                <td align="center">10</td>
                                <td>TNF Buckland Pant</td>
                                <td>2</td>
                                <td><input type="text" class="form-control" id="qtyCek" name="qtyCek" value="<?php echo set_value('qtyCek');?>"></td>
                                <td><input type="text" class="form-control" id="note" name="note" value="<?php echo set_value('note');?>"></td>
                                                           
                            </tr>
                            <tr>
                                <td align="center">11</td>
                                <td>TNF Buckland Pant</td>
                                <td>3</td>
                                <td><input type="text" class="form-control" id="qtyCek" name="qtyCek" value="<?php echo set_value('qtyCek');?>"></td>
                                <td><input type="text" class="form-control" id="note" name="note" value="<?php echo set_value('note');?>"></td>
                                                           
                            </tr>


                        </tbody>
                    </table>
                    <div class="form-group">
                        <div class="col-xs-offset-8">
                            <?php echo form_submit('submit', 'Simpan','class="btn btn-primary"'); ?>
                            <?php echo form_reset('button', 'Reset', 'class="btn btn-default"'); ?> 
                            
                        </div>                              
                    </div>
                </div>

		</div>



        <div class="panel panel-default paper-shadow" data-z="0.5">
            <div class="table-responsive">
                 <table class="table table-hover">
                        <thead>                            
                            <tr>
                                <td>ID S.O.</td>
                                <td>Tanggal</td>
                                <td>User Check</td>
                            </tr>
                        </thead>
                        <tr>
                            <td><a href="http://localhost/inventori/Asset/stockOpnameDetail">001</a></td>
                            <td>25 Agustus 2015</td>
                            <td>Staff</td>
                        </tr>
            </div>
        </div>
    
	</div> 



</div>		