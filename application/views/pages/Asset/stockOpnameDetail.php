<div class="container-fluid">
	<div class="page-section">
		<h3 class="text-display-1 margin-none">Stock Opname Detail : 001</h3>
        <h3 class="text-display-1 margin-none"><small> 25 Agustus 2015</small></h3>
	</div>

    <div class="row" data-toggle="isotope">
        
        <div class="item col-lg-6">
            <div class="panel panel-default paper-shadow" data-z="0.5">
                <a class="btn btn-default btn-primary" href="/inventori/Asset/stockOpname" role="button"><h5>Back</h5></a>
            </div>
        </div>
    </div>

	<div class="row" data-toggle="isotope">
		<div class="panel panel-default paper-shadow" data-z="0.5">
			<div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td class="info">ID Barang</td>
                                <td class="info">Nama Barang</td>
                                <td class="info">Qty Syst</td>
                                <td class="info">Qty Check</td>
                                <td class="info">Note</td>
                            </tr>
                        </thead>
                        <?php 
                            
                            foreach($data as $row)
                            {


                        ?>
                        <tbody>
                            <tr>
                            	<td align="center"><?php echo $row->id_product; ?></td>
                                <td ><?php echo $row->nama_product;?></td>
                                <td align="center"><?php echo $row->qty_system;?></td>
                                <td align="center"><?php echo $row->qty_check;?></td>
                               
                                <td><?php echo $row->note;?></td>
                            </tr>
                            <?php 
                                } 
                            ?>
                        </tbody>
                    </table>
                </div>
		</div>
    
	</div> 



</div>		