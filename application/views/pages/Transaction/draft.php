<div class="container-fluid">

	<div class="page-section">
		<h1 class="text-display-1 margin-none">DRAFT </h1>
		
	</div>


	<div class="row" data-toggle="isotope">
		<div class="item col-xs-12 col-lg-6">
			<div class="panel panel-default paper-shadow" data-z="0.5">
				<div class="">

				</div>
				<div>
					<p>ID SOLD : 5</p>
					<p>Pelanggan : Yulian Tri Saputra</p>
				</div>
				<div class="table-responsive">
					<table class="table table-hover">
						<th class="info">
							<td class="info">Nama</td>
							<td class="info">Deskripsi</td>
							<td class="info">Size</td>
							<td class="info">Warna</td>
							<td class="info">Qty</td>
							<td class="info">Harga Modal</td>
							<td class="info">Harga Jual</td>
							<td class="info"> </td>

						</th>
						
						<tr>
							<td>1</td>
							<td>TNF Arroyyo Pant</td>
							<td>- Lingkar Pinggang : 100cm <br>- Canvas Cargo</td>
							<td>31 Euro</td>
							<td>Grey Steel</td>
							<td>1</td>
							<td>180000</td>
							<td>280000</td>
							<td>
								<a href="#">
									<span class="glyphicon glyphicon-trash"></span>
								</a>
								
							</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Lafuma Inner</td>
							<td>- K-K : 50cm <br>- Inner Dacron</td>
							<td>S Euro</td>
							<td>Black</td>
							<td>1</td>
							<td>220000</td>
							<td>350000</td>
							<td>
								<a href="#">
									<span class="glyphicon glyphicon-trash"></span>
								</a>
								
							</td>
						</tr>
						<tr>
							<td colspan="7" align="right">Total (Rp.):</td>
							<td>630000</td>
						</tr>

					</table>

					<div class="panel-footer text-right">					
                   		<a href="#" class="btn btn-primary paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">CONTINUE</a>
           				<a href="#" class="btn btn-white paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">DELETE</a>
           			</div>
				</div>
			</div>
				<div class="panel panel-default paper-shadow" data-z="0.5">
				<div class="">

				</div>
				<div>
					<p>ID SOLD : 6</p>
					<p>Pelanggan : M. Alghifari </p>
				</div>
				<div class="table-responsive">
					<table class="table table-hover">
						<th class="info">
							<td class="info">Nama</td>
							<td class="info">Deskripsi</td>
							<td class="info">Size</td>
							<td class="info">Warna</td>
							<td class="info">Qty</td>
							<td class="info">Harga Modal</td>
							<td class="info">Harga Jual</td>
							<td class="info"> </td>

						</th>
						
						<tr>
							<td>1</td>
							<td>TNF Inner Special Edition Boston Mills Brandy Wine </td>
							<td>- K-K : 56cm <br>- Inner Dacron</td>
							<td>M Euro</td>
							<td>Green</td>
							<td>1</td>
							<td>280000</td>
							<td>350000</td>
							<td>
								<a href="#">
									<span class="glyphicon glyphicon-trash"></span>
								</a>
								
							</td>
						</tr>
						<tr>
							<td colspan="7" align="right">Total (Rp.):</td>
							<td>350000</td>
						</tr>

					</table>

					<div class="panel-footer text-right">					
                   		<a href="#" class="btn btn-primary paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">CONTINUE</a>
                   		<a href="#" class="btn btn-white paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">DELETE</a>
           			</div>
				</div>
			</div>
				<div class="panel panel-default paper-shadow" data-z="0.5">
				
				<div>
					<p>ID SOLD : 7</p>
					<p>Pelanggan : Aisa Asatia</p>
				</div>
				<div class="table-responsive">
					<table class="table table-hover">
						<th class="info">
							<td class="info">Nama</td>
							<td class="info">Deskripsi</td>
							<td class="info">Size</td>
							<td class="info">Warna</td>
							<td class="info">Qty</td>
							<td class="info">Harga Modal</td>
							<td class="info">Harga Jual</td>
							<td class="info"> </td>

						</th>
						
						<tr>
							<td>1</td>
							<td>Lafuma LD Green Light 1/2 Zip </td>
							<td>- K-K : 54cm <br>- Polartec</td>
							<td>L Euro</td>
							<td>Black</td>
							<td>1</td>
							<td>85000</td>
							<td>200000</td>
							<td>
								<a href="#">
									<span class="glyphicon glyphicon-trash"></span>
								</a>
								
							</td>
						</tr>						
						<tr>
							<td colspan="7" align="right">Total (Rp.):</td>
							<td>200000</td>
						</tr>

					</table>

					<div class="panel-footer text-right">					
                   		<a href="#" class="btn btn-primary paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">CONTINUE</a>
           				<a href="#" class="btn btn-white paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">DELETE</a>
           			</div>
				</div>
			</div>
		</div>





</div>