<div class="container-fluid">
<script>
	$(document).ready(function(){
		
		$('#kdBarang').on("change",function(){
			$.ajax({
				type:"POST",
				dataType:'JSON',
				url:'http://localhost/inventori/Product/ajaxGetProductByID/'+$('#kdBarang').val(),
				success: function(data){
						//$('#brand').val(data[0]['brand']);
						$('#namaBarang').val(data[0]['nama_product']);
						$('#deskripsi').val(data[0]['deskripsi']);
						//$('#kategori').val(data[0]['kategori']);
						$('#size').val(data[0]['size']);
						$('#warna').val(data[0]['warna']);
						$('#qty').val(data[0]['qty']);
						$('#hargaM').val(data[0]['hargaM'])

				}
			});				
		});

	}); 
</script>
	<div class="page-section">
		<h1 class="text-display-1 margin-none">ADD ITEM</h1>
	</div>

	<script src="<?php echo base_url(); ?>js/app/app.js"></script>

	<div class="row" data-toggle="isotope">
		<div class="item col-xs-12 col-lg-6">
			<div class="panel panel-default paper-shadow" data-z="0.5">
				

				<?php 
					$attribute = array(
								'class'=>'form-horizontal',
								'role'=>'form'

						);
					echo form_open_multipart('Transaction/addSoldProduct',$attribute);
				?>
					
					<div class="form-group">
				        <label for="inDate" class="col-xs-3 control-label">Date</label>
				        <div class="col-xs-5 date">
				            <div class="input-group date" id="inDate">
				                <input type="text" class="form-control" value="<?php echo set_value('inDate');?>">
				                <span class="input-group-addon">
				                	<span class="glyphicon glyphicon-calendar"></span>
				                </span>
				                <script type="text/javascript">
						            $(document).ready(function(){
						            	 $('#inDate').datepicker({
						                    "dateFormat":"yyyy-mm-dd"
						                });
						            });
						       </script>
				            </div>
				        </div>				        
				    </div>

					<div class="form-group">
						<br>
						<label for="kdBarang" class="col-xs-3 control-label">Input Kode Barang</label>
						<div class="col-xs-8">
							<input type="text" id="kdBarang" class="form-control">
						</div>						
					</div>

					<div class="form-group">
						<label for="brand" class="col-xs-3 control-label">Brand</label>
						<div class="col-xs-8">
							<input type="text" id="brand" class="form-control" disabled value="The North Face">
						</div>						
					</div>

					<div class="form-group">
						<label for="namaBarang" class="col-xs-3 control-label">Nama Barang</label>
						<div class="col-xs-8">
							<input type="text" class="form-control" id="namaBarang" disabled>
						</div>						
					</div>

					<div class="form-group">
						<label for="deskripsi" class="col-xs-3 control-label">Deskripsi Barang</label>
						<div class="col-xs-8">
							<textarea id="deskripsi" rows="3" class="form-control" disabled></textarea>
						</div>						
					</div>

					<div class="form-group">
						<label for="kategori" class="col-xs-3 control-label">Kategori</label>
						<div class="col-xs-8">
							<input type="text" class="form-control" id="kategori" value="Jacket Waterproof" disabled>
						</div>						
					</div>

					<div class="form-group">
						<label for="size" class="col-xs-3 control-label">Size</label>
						<div class="col-xs-8">
							<input type="text" class="form-control" id="size" disabled>
						</div>						
					</div>

					<div class="form-group">
						<label for="warna" class="col-xs-3 control-label">Warna</label>
						<div class="col-xs-8">
							<input type="text" class="form-control" id="warna" disabled>
						</div>				
					</div>

					<div class="form-group">
						<label for="qty" class="col-xs-3 control-label">Qty</label>
						<div class="input-group">
							<div class="col-xs-2">
								<input type="text" class="form-control" id="qtyOut">
							</div>		
							
							<div class="col-xs-3 input-group">
							<span class="input-group-addon"><strong>/</strong></span>							
							<input type="text" class="form-control" id="qty" disabled> 
							</div>			
							
								
						</div>
						
					</div>
					<!--
					<div class="form-group">
						<label for="hargaD" class="col-xs-3 control-label">Harga Modal</label>
						<div class="col-xs-8">
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								<input type="text" class="form-control" id="hargaM" disabled>
							</div>							
						</div>				
					</div>
				-->

					<div class="form-group">
						<label for="hargaD" class="col-xs-3 control-label">Harga Deal</label>
						<div class="col-xs-8">
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								<input type="text" class="form-control" id="hargaD">
							</div>							
						</div>				
					</div>
					
					

					<div class="form-group">
						<div class="col-xs-offset-3 col-xs-8">
							<button type="submit" class="btn btn-primary">ADD</button>
						</div>								
					</div>

					
				</form>
			</div>

		</div>

		<div class="item col-xs-12 col-lg-6">	
			<div class="panel panel-default paper-shadow" data-z="0.5">		
				<div class="table-responsive">
					<table class="table table-hover">
						<tr>
							<td class="info col-xs-1">ID Barang</td>
							<td class="info col-xs-3">Nama</td>
							<td class="info col-xs-1">Size</td>
							<td class="info col-xs-1">Qty</td>
							<!--<td class="info">Harga Modal</td>-->
							<td class="info col-xs-2">Harga Deal</td>
							<td class="info col-xs-1"> </td>

						</tr>
						<tr>
							<td>5</td>
							<td>TNF Dubs Insulated Snow Jacket</td>
							<td>L Euro</td>
							<td>1</td>
							<!--<td>350000</td>-->
							<td>465000</td>
							<td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
						</tr>								

					</table>				

				</div>

<!--				
				<div class="item col-xs-12 col-lg-6">
					<div class="panel panel-default paper-shadow" data-z="0.5">

					<?php 
						#$attribute = array(
						#			'class'=>'form-horizontal',
						#			'role'=>'form'

						#	);
						#echo form_open_multipart('Transaction/addSoldProduct',$attribute2);
					?>

					<form>
						<div class="form-group">
							<label for="nama_pelanggan" class="control-label">Nama Pelanggan</label>
						<div class="col-xs-8">
							<input type="text" id="nama_pelanggan" class="form-control" >
						</div>						
					</div>

					<div class="form-group">
						<label for="no_hp" class="control-label">No. HP </label>
						<div class="col-xs-8">
							<input type="text" class="form-control" id="no_hp">
						</div>						
					</div>

					<div class="form-group">
						<label for="alamat" class="control-label">Alamat </label>
						<div class="col-xs-8">
							<textarea class="form-control" id="alamat" rows="3"> </textarea>
						</div>						
					</div>


					</form>

					</div>
-->
				</div>
							
				<div class="panel-footer text-right">						
					<a href="#" class="btn btn-white paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">Save Draft</a>
	                <a href="#" class="btn btn-primary paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">Finish</a>
	           	</div>			
			</div>

		</div>
	</div>


</div>