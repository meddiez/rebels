<div class="container-fluid">

	<div class="page-section">
		<h1 class="text-display-1 margin-none">SOLD Confirmation</h1>
		<h2 class="text-display-1 margin-none"><small>ID SOLD : 5</small></h2>
		<h2 class="text-display-1 margin-none"><small>Pelanggan : Yulian Tri Saputra</small></h2>
		
		
	</div>

	<div class="row" data-toggle="isotope">
		<div class="item col-xs-12 col-lg-6">
			<div class="panel panel-default paper-shadow" data-z="0.5">
				<div class="">

				</div>

				<div class="table-responsive">
					<table class="table table-hover">
						<th class="info">
							<td class="info">Nama</td>
							<td class="info">Deskripsi</td>
							<td class="info">Size</td>
							<td class="info">Warna</td>
							<td class="info">Qty</td>
							
							<td class="info">Harga Jual</td>
							<td class="info"> </td>

						</th>
						
						<tr>
							<td>1</td>
							<td>TNF Arroyyo Pant</td>
							<td>- Lingkar Pinggang : 100cm <br>- Canvas Cargo</td>
							<td>31 Euro</td>
							<td>Grey Steel</td>
							<td>1</td>
							
							<td>280000</td>
							<td>
								<a href="#">
									<span class="glyphicon glyphicon-trash"></span>
								</a>
								
							</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Lafuma Inner</td>
							<td>- K-K : 50cm <br>- Inner Dacron</td>
							<td>S Euro</td>
							<td>Black</td>
							<td>1</td>
						
							<td>350000</td>
							<td>
								<a href="#">
									<span class="glyphicon glyphicon-trash"></span>
								</a>
								
							</td>
						</tr>
						
						<tr>
							<td colspan="7" align="right"> Total : </td>
							<td colspan="2" align="right"> 630000</td>
						</tr>
					
						<tr>
							<td colspan="7" align="right"> No. HP : </td>
							<td colspan="2" align="right"><input type="text" class="form-control" id="payment"></td>
						</tr>
						<tr>
							<td colspan="7" align="right"> Alamat : </td>
							<td colspan="2" align="right"> <textarea class="form-control" id="Alamat" rows="3"> </textarea></td>
						</tr>
						<tr>
							<td colspan="7" align="right"> Ongkir : </td>
							<td colspan="2" align="right"><input type="text" class="form-control" id="ongkir"></td>
						</tr>
						<tr>
							<td colspan="7" align="right"> Pembayaran : </td>
							<td colspan="2" align="right"><input type="text" class="form-control" id="payment"></td>
						</tr>
						<tr>
							<td colspan="7" align="right"> Resi :</td>
							<td colspan="2" > <input type="text" class="form-control" id="Resi"></td>
						</tr>


					</table>

					<div class="panel-footer text-right">
						<!--<a href="#" class="btn btn-white paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">Save Draft</a>--> 
                   		<a href="#" class="btn btn-primary paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">Finish</a>
           			</div>
				</div>
			</div>
		</div>
	</div>

</div>