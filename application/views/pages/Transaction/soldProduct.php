<div class="container-fluid">

	<div class="page-section">
		<h1 class="text-display-1 margin-none">SOLD PRODUCTS</h1>
	</div>

	<div class="row" data-toggle="isotope">
		<div class="item col-lg-6">
			<div class="panel panel-default paper-shadow" data-z="0.5">
				<a class="btn btn-default btn-primary" href="/inventori/Transaction/addSoldProduct" role="button"><h5>+ Add Sold Item +</h5></a>
			</div>
		</div>

		<div class="item col-xs-12 col-lg-6">
			<div class="panel panel-default paper-shadow" data-z="0.5">

				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<td class="info">Purchased ID</td>							
							<td class="info">Nama Produk</td>
							<td class="info">Size</td>
							<td class="info">Warna</td>
							<td class="info">Qty</td>
							<td class="info">Harga Deal</td>
							<td class="info">Resi</td>

						</thead>

						<tr margin="auto">
							<td rowspan="2">
								<center>1<br>
								Aisa Asatia</center>
							</td>
							<td>Columbia T-Shirt V-Neck Organic</td>
							<td>S Euro</td>
							<td>Peach Red</td>
							<td>1</td>
							<td>90000</td>
							<td rowspan="2"><center><br><br>CGKN201301203423</center></td>
						</tr>
						<tr margin="auto">
							<td>JWS Paw Print T-Shirt</td>
							<td>M Euro</td>
							<td>Black Steel</td>
							<td>1</td>
							<td>100000</td>
						</tr>
						<tr class="warning">
							<td> 2015 - Maret - 12</td>
							<td colspan="4" align="right">Payment =</td>
							<td colspan="2"> Rp. 200.000</td>
						</tr>

						<tr margin="auto">
							<td margin="auto">
								<center>2<br>
								Azimi Aviv</center>
							</td>
							<td>Mountain Hardwear South Col 70</td>
							<td>70L</td>
							<td>Black-Gold</td>
							<td>1</td>
							<td>3.000.000</td>
							<td><center>COD</center></td>
						</tr>
						<tr></tr>
						<tr class="warning">
							<td> 2015 - September - 7</td>
							<td colspan="4" align="right">Payment =</td>
							<td colspan="2"> Rp. 3.000.000</td>
						</tr>

						<tr>
							<td margin="auto">
								<center>3<br>
								M. Ghazalli</center>
							</td>
							<td>Craghopper Kiwi Gore-Tex</td>
							<td>L Euro</td>
							<td>Blue Navy</td>
							<td>1</td>
							<td>667.000</td>
							<td><center>CGKI20133224058</center></td>
						</tr>
						<tr></tr>
						<tr class="warning">
							<td> 2015 - Oktober - 28</td>
							<td colspan="4" align="right">Payment =</td>
							<td colspan="2"> Rp. 675.000</td>
						</tr>


						<tr>
							<td rowspan="3" margin="auto">
								<br><br><center>4<br>
								Dali Shoufari Arzie</center>
							</td>
							<td>Eider Waterproof Pants</td>
							<td>S Euro</td>
							<td>Black</td>
							<td>1</td>
							<td>380000</td>
							<td rowspan="3"><center><br><br><br>CGKN2013245803</center></td>
						</tr>
						<tr>
							<td>Matras Demonteen</td>
							<td>Single</td>
							<td>Black</td>
							<td>1</td>
							<td>45000</td>
						</tr>
						<tr>
							<td>Ziener Glove</td>
							<td>9</td>
							<td>Black - Red List</td>
							<td>1</td>
							<td>120000</td>
						</tr>
						<tr class="warning">
							<td> 2015 - November - 13</td>
							<td colspan="4" align="right">Payment =</td>
							<td colspan="2"> Rp. 555.000</td>
						</tr>

						<tr>
							<td rowspan="2" margin="auto">
								<center>5<br>
								Yulian Tri Saputra</center>
							</td>
							<td>TNF Arroyyo Pant</td>
							<td>31</td>
							<td>Grey Steel</td>
							<td>1</td>
							<td>280000</td>
							<td rowspan="3"><center><br>CGKI2015331456632</center></td>
						</tr>
						<tr>
							<td>Lafuma Inner</td>
							<td>S Euro</td>
							<td>Black</td>
							<td>1</td>
							<td>350000</td>
						</tr>
						
						<tr class="warning">
							<td> 2015 - Desember - 7</td>
							<td colspan="4" align="right">Payment =</td>
							<td colspan="2"> Rp. 638.000</td>
						</tr>


					</table>
				</div>
			</div>
		</div>
	</div>


</div>