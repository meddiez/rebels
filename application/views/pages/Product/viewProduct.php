<div class="container-fluid">

	<div class="page-section">
		<h1 class="text-display-1 margin-none">ALL PRODUCTS</h1>
		 <p class="text-subhead">List All Products.</p>
	</div>
	
	<div class="row" data-toggle="isotope">
		
		<div class="item col-lg-6">
			<div class="panel panel-default paper-shadow" data-z="0.5">
				<a class="btn btn-default btn-primary" href="/inventori/Product/addProduct" role="button"><h5>+ NEW +</h5></a>
			
			<div class=" item col-xs-6 input-group">
				<input type="text" class="form-control" style="width:300px;" id="cari"  name="cari" placeholder="input nama barang" value= "<?php echo set_value('cari');?>">
				<a class="input-group-addon glyphicon glyphicon-search"></a>

			</div>
			</div>
		</div>
	</div>
	
		<div class="alert alert-danger" role="alert">
		  <strong>Sukses!</strong> Produk #1 Berhasil Dihapus.
		</div>
	
	<div class="row" data-toggle="isotope">	
		<div class="item col-xs-12 col-lg-6">
			<div class="panel panel-default paper-shadow" data-z="0.5">

				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<td class="info">ID</td>
							<td class="info"> </td>
							<td class="info">Nama</td>
							<td class="info">Deskripsi</td>
							<td class="info">Size</td>
							<td class="info">Warna</td>
							<td class="info">Qty</td>
							<!--<td class="info">Harga Modal</td>-->
							<td class="info">Harga Jual</td>
							<td class="info">Action</td>
						</thead>
						<?php 
							
							foreach($data as $row)
							{


						?>
						<tbody>
							<tr>
								<td><?php echo $row->id_product; ?></td>
								<td><img class="img-square width-50" src="<?php echo base_url().'/images/PIC Web/'.$row->image; ?>"/> </td>
								<td><?php echo $row->nama_product;?></td>
								<td><?php echo $row->deskripsi;?></td>
								<td><?php echo $row->size;?></td>
								<td><?php echo $row->warna;?></td>
								<td><?php echo $row->qty;?></td>
								<!--<td><?php echo $row->hargaM;?></td>-->
								<td><?php echo $row->hargaJ;?></td>
								<td>
									<a href="<?php echo base_url() . "Product/editProduct/" . $row->id_product; ?>">
	          							<span class="glyphicon glyphicon-pencil" ></span>
	       							</a>
	       							<a href="<?php echo base_url() . "Product/deleteProduct/" . $row->id_product; ?>">
										<span class="glyphicon glyphicon-trash"></span>
									</a>
									<?php echo "&nbsp; &nbsp;"; ?>
									
								</td>
							</tr>
							<?php 
								} 
							?>
						</tbody>

					</table>
				</div>

			</div>

			<ul class="pagination margin-top-none">
            <li><a href="#">&laquo;</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&raquo;</a></li>
        </ul>

		</div>
	</div>




</div>