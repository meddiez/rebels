<div class="container-fluid">

	<div class="page-section">
		<h3 class="text-display-1 margin-none">EDIT PRODUCT</h3>
	</div>

	<div class="row data-toggle isotope">
		<div class="item col-xs-12 col-lg-6">
			<div class="panel panel-default paper-shadow" data-z="0.5">
				<div class="panel-body">
					<?php 
						$attribute = array(
							'class'=>'form-horizontal',
							'role'=>'form',
							);
				echo form_open_multipart('Product/saveProduct/'.$dataId[0]->id_product,$attribute);?>
					<div class="form-group">
						<br>
						<label for="inputPhoto" class="col-xs-3 control-label">Add Photo</label>
						<div class="col-xs-8">
							<!--<?php
									$attribute = array(
											'type' 	=> 'file',
											'id' 	=> 'image',
											'name'	=> 'image',
											'class' => 'form-control'

										);
									echo form_input($attribute, set_value('image'));
							?>-->
							<input type="file" id="image" class="form-control" value="<?php echo $dataId[0]->image?>">
						</div>						
					</div>

					<div class="form-group">
						<label for="idBarang" class="col-xs-3 control-label">ID Barang</label>
						<div class="col-xs-8">
							<input type="text" class="form-control" id="idBarang" value="<?php echo $dataId[0]->id_product;?>" disabled>
						</div>											
					</div>

					<div class="form-group">
						<label for="brand" class="col-xs-3 control-label">Brand</label>
						<div class="col-xs-8">
						
							<?php

								$options = array();
							
								foreach ($brand as $row) 
								{
									$options[$row->id_brand] = $row->brand ;
									
								
									//array_push($options, $row->id_brand => $row->brand);
									
								}
								
							?>
							<?php echo form_dropdown('brand', $options, $dataId[0]->id_brand,'class="form-control" id="brand" onchange="this.form.submit();"',$attribute); ?>
						
						</div>						
					</div>


					<div class="form-group">
						<label for="namaBarang" class="col-xs-3 control-label">Nama Barang</label>
						<div class="col-xs-8">
								<?php
									# $attribute = array(
									#		'type'	=> 'text',
									#		'id'	=> 'namaBarang',
									#		'name'	=> 'namaBarang',
									#		'class' => 'form-control'

									#	);
									#echo form_input($attribute, set_value='namaBarang');
								?>
							<input type="text" class="form-control" id="namaBarang" name="namaBarang" value="<?php echo $dataId[0]->nama_product;?>"> 
						</div>						
					</div>

					<div class="form-group">
						<label for="deskripsi" class="col-xs-3 control-label">Deskripsi Barang</label>
						<div class="col-xs-8">
							<?php
								#	$attribute = array(
								#			'type'  => 'textarea',											
								#			'id' 	=> 'deskripsi',
								#			'name'	=> 'deskripsi',
								#			'rows'  => '3',
								#			'class' => 'form-control'

								#		);
								#	echo form_input($attribute, set_value='deskripsi');
							?>
							<textarea id="deskripsi" rows="3" class="form-control"  name="deskripsi" value="<?php echo $dataId[0]->deskripsi;?>"><?php echo $dataId[0]->deskripsi;?></textarea> 
						</div>						
					</div>

					
					<div class="form-group">
						<label for="kategori" class="col-xs-3 control-label">Kategori</label>
						<div class="col-xs-8">
							
								<?php 
									$options = array();
									foreach ($kategori as $row) 
									{
								
									$options[$row->id_kategori] = $row->kategori ;									
								
									}
									echo form_dropdown('kategori', $options, $dataId[0]->id_kategori,'class="form-control" id="kategori" onchange="this.form.submit();"',$attribute);
								?>
							
						</div>						
					</div>
					

					<div class="form-group">
						<label for="size" class="col-xs-3 control-label">Size</label>
						<div class="col-xs-8">
								<?php
									#$attribute = array(
									#		'type' => 'text',
									#		'id' => 'size',
									#		'name'	=> 'size',
									#		'class' => 'form-control'
									#	);
									#echo form_input($attribute, set_value='size');
							?>
							<input type="text" class="form-control" id="size"  name="size" value="<?php echo $dataId[0]->size;?>">
						</div>						
					</div>

					<div class="form-group">
						<label for="warna" class="col-xs-3 control-label">Warna</label>
						<div class="col-xs-8">
								<?php
								#	$attribute = array(
								#			'type'	=> 'text',
								#			'id' 	=> 'warna',
								#			'name'	=> 'warna',
								#			'class' => 'form-control'
								#		);
								#	echo form_input($attribute,set_value='warna');
							?>
							<input type="text" class="form-control" id="warna"  name="warna" value="<?php echo $dataId[0]->warna;?>"> 
						</div>				
					</div>

					<div class="form-group">
						<label for="qty" class="col-xs-3 control-label">Qty</label>
						<div class="col-xs-8">
								<?php
								#	$attribute = array(
								#			'type'	 => 'text',
								#			'id'	 => 'qty',
								#			'name'	 => 'qty',
								#			'class'  => 'form-control'
								#		);
								#	echo form_input($attribute,set_value='qty');
							?>
							<input type="text" class="form-control" id="qty"  name="qty" value="<?php echo $dataId[0]->qty;?>"> 
						</div>				
					</div>

					<!--
					<div class="form-group">
						<label for="hargaB" class="col-xs-3 control-label">Harga Modal</label>
						<div class="col-xs-8">
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
									<?php
									#	$attribute = array(
									#		'type'  => 'text',
									#		'id'	=> 'hargaM',
									#		'name'	=> 'hargaM',
									#		'class' => 'form-control'
									#	);
									#echo form_input($attribute, set_value='hargaB');
									?>
								<input type="text" class="form-control" id="hargaM"  name="hargaM" value="<?php echo $dataId[0]->hargaM;?>">
							</div>
							
						</div>				
					</div>
				-->

					<div class="form-group">
						<label for="hargaJ" class="col-xs-3 control-label">Harga Jual</label>
						<div class="col-xs-8">
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
									<?php
									#	$attribute = array(
									#		'type' => 'text',
									#		'id' => 'hargaJ',
									#		'name'	=> 'hargaJ',
									#		'class' => 'form-control'
									#	);
									#	echo form_input($attribute,set_value='hargaJ');
									?>
								<input type="text" class="form-control" id="hargaJ"  name="hargaJ" value="<?php echo $dataId[0]->hargaJ;?>"> 
							</div>
							
						</div>				
					</div>

					<div class="form-group">
						<div class="col-xs-offset-3 col-xs-8">
							<?php echo form_submit('submit', 'SAVE','class="btn btn-primary"'); ?>
							<?php echo form_reset('button', 'Reset', 'class="btn btn-default"'); ?> 
							
						</div>								
					</div>
					<?php echo form_close();?>
				</div>

			</div>
		</div>
	</div>


</div>