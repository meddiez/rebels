<div class="container-fluid">

    <div class="page-section">
        <h1 class="text-display-1 margin-none">DASHBOARD</h1>
    </div>

    

    <div class="row" data-toggle="isotope">
        <div class="item col-xs-12 col-lg-6">
            <div class="panel panel-default paper-shadow" data-z="0.5">
                <div class="panel-heading">
                    <h4 class="text-headline margin-none">Low Quantity Product</h4>
                    <p class="text-subhead text-light">Single Product</p>
                </div>
                <ul class="list-group">
                    <li class="list-group-item media v-middle">
                        <div class="media-body">
                            <img src="<?php echo base_url();?>/images/Pic Web/tnf cassius.jpg" alt="tnfCassius" class="img-square width-50">
                            <a href="app-take-course.html" class="text-subhead list-group-link">The North Face Stinson Jacket</a>
                        </div>
                        <div class="media-right">
                            <!--<div class="progress progress-mini width-100 margin-none">
                                <div class="progress-bar progress-bar-red-300" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:45%;">
                                </div>

                            </div>-->
                            1 pc

                        </div>
                    </li>
                    <li class="list-group-item media v-middle">
                        <div class="media-body">
                            <img src="<?php echo base_url();?>/images/Pic Web/columbia_T-shirt.jpg" alt="tnfCassius" class="img-square width-50">
                            <a href="app-take-course.html" class="text-subhead list-group-link">Columbia T-Shirt V-Neck Organic</a>
                        </div>
                        <div class="media-right">
                            <!--<div class="progress progress-mini width-100 margin-none">
                                <div class="progress-bar progress-bar-red-300" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:75%;">
                                </div>
                            </div>-->
                            1 pc
                        </div>
                    </li>
                    <li class="list-group-item media v-middle">
                        <div class="media-body">
                            <img src="<?php echo base_url();?>/images/Pic Web/napapijri-rainforest-slim-summer-b31.jpg" alt="napapijriRainforest" class="img-square width-50">
                            <a href="app-take-course.html" class="text-subhead list-group-link">Napapijri Rainforest Waterproof Jacket</a>
                        </div>
                        <div class="media-right">
                            <!--<div class="progress progress-mini width-100 margin-none">
                                <div class="progress-bar progress-bar-green-300" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100" style="width:25%;">
                                </div>
                            </div>-->
                            1 pc
                        </div>
                    </li>
                </ul>
                <div class="panel-footer text-right">
                    <a href="#" class="btn btn-white paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#"> View all</a>
                </div>

            </div>
        </div>
        <div class="item col-xs-12 col-lg-6">
            <div class="panel panel-default paper-shadow" data-z="0.5">
                <div class="panel-body">
                    <h4 class="text-headline margin-none">Total Sales</h4>
                    <p class="text-subhead text-light">Total sales last month per Brand</p>
                     <ul class="list-group">
                    
                    <li class="list-group-item media v-middle">
                        <div class="media-body">
                            <a href="app-take-course.html" class="text-subhead list-group-link">Columbia</a>
                        </div>
                        <div class="media-right">
                            <!--<div class="progress progress-mini width-100 margin-none">
                                <div class="progress-bar progress-bar-red-300" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:75%;">
                                </div>
                            </div>-->
                            2 pc
                        </div>
                    </li>
                     <li class="list-group-item media v-middle">
                        <div class="media-body">
                            <a href="app-take-course.html" class="text-subhead list-group-link">Jack Wolfskin</a>
                        </div>
                        <div class="media-right">
                            <!--<div class="progress progress-mini width-100 margin-none">
                                <div class="progress-bar progress-bar-green-300" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100" style="width:25%;">
                                </div>
                            </div>-->
                            6 pc
                        </div>
                    </li>
                    <li class="list-group-item media v-middle">
                        <div class="media-body">
                            <a href="app-take-course.html" class="text-subhead list-group-link">Millet</a>
                        </div>
                        <div class="media-right">
                            <!--<div class="progress progress-mini width-100 margin-none">
                                <div class="progress-bar progress-bar-green-300" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100" style="width:25%;">
                                </div>
                            </div>-->
                            1 pc
                        </div>
                    </li>
                    <li class="list-group-item media v-middle">
                        <div class="media-body">
                            <a href="app-take-course.html" class="text-subhead list-group-link">The North Face</a>
                        </div>
                        <div class="media-right">
                            <!--<div class="progress progress-mini width-100 margin-none">
                                <div class="progress-bar progress-bar-red-300" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:45%;">
                                </div>
                            </div>-->
                            3 pc
                        </div>
                    </li> 
                </ul>
                </div>
            </div>
            <!--
            <div class="panel panel-default paper-shadow" data-z="0.5">
                <div class="panel-heading">
                    <h4 class="text-headline">Asset Value</h4>
                    <p class="text-subhead text-light">Asset total by Brand</p>
                </div>
                
                 
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td class="info">Brand</td>
                                <td class="info">Kategori</td>
                                <td class="info">Qty</td>
                                <td class="info">Value</td>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td>Berghaus</td>
                                <td>Jacket Waterproof</td>
                                <td>2</td>
                                <td>850.000</td>                               
                            </tr>  
                            <tr>
                                <td>Columbia</td>
                                <td>T-Shirt</td>
                                <td>9</td>
                                <td>675.000</td>                               
                            </tr>                                                            
                           
                            <tr>
                                <td>Jack Wolfskin</td>
                                <td>Jacket Waterproof</td>
                                <td>2</td>
                                <td>1.000.000</td>                               
                            </tr> 
                             <tr>
                                <td>Jack Wolfskin</td>
                                <td>T-Shirt</td>
                                <td>8</td>
                                <td>1.000.000</td>                               
                            </tr>   
                            <tr>
                                <td>Napapijri</td>
                                <td>T-Shirt</td>
                                <td>6</td>
                                <td>360.000</td>                               
                            </tr>
                            <tr>
                                <td>The North Face</td>
                                <td>Celana</td>
                                <td>8</td>
                                <td>2.000.000</td>                               
                            </tr>  

                            <tr>
                                <td>The North Face</td>
                                <td>Jacket Inner</td>
                                <td>10</td>
                                <td>3.350.000</td>                               
                            </tr>

                            <tr>
                                <td>Trespass</td>
                                <td>Jacket Waterproof</td>
                                <td>5</td>
                                <td>1.350.000</td>                               
                            </tr>

                                               
                                                       
                        </tbody>
                        <tfoot >
                            <td colspan="3" align="right" class="info"> Total (Rp.)</td>
                            <td class="info">9.585.000</td>
                        </tfoot>
                    </table>
                </div>
            </div>
            -->
               
            </div>
        </div>
     </div>   

    <br/>

</div>