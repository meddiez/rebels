-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2016 at 06:08 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_roe`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `id_brand` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(25) NOT NULL,
  PRIMARY KEY (`id_brand`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id_brand`, `brand`) VALUES
(1, 'The North Face'),
(2, 'Jack Wolfskin'),
(3, 'Columbia'),
(4, 'Trespass'),
(5, 'Millet'),
(6, 'Salomon'),
(7, 'Buffalo'),
(8, 'Karrimor'),
(9, 'Lafuma'),
(10, 'Iguana'),
(11, 'Berghaus'),
(12, 'Old Navy'),
(13, 'Rugged');

-- --------------------------------------------------------

--
-- Table structure for table `detail_stok_opname`
--

CREATE TABLE IF NOT EXISTS `detail_stok_opname` (
  `id_so` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `qty_system` int(11) NOT NULL,
  `qty_check` int(11) NOT NULL,
  `note` text,
  PRIMARY KEY (`id_so`,`id_product`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `draft`
--

CREATE TABLE IF NOT EXISTS `draft` (
  `id_draft` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `nama_pembeli` varchar(50) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `alamat` text,
  PRIMARY KEY (`id_draft`,`id_product`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `header_stok_opname`
--

CREATE TABLE IF NOT EXISTS `header_stok_opname` (
  `id_so` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `so_cek` date NOT NULL,
  PRIMARY KEY (`id_so`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(25) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`) VALUES
(1, 'Celana'),
(2, 'Jacket Inner / Polar'),
(3, 'T-Shirt'),
(4, 'Jacket Waterproof'),
(5, 'Fleece / Quick Dry '),
(6, 'Celana Waterproof'),
(7, 'Kemeja'),
(8, 'Sepatu '),
(9, 'Sepatu Waterproof'),
(10, 'Carrier'),
(11, 'Matras'),
(12, 'Sleeping Bag'),
(13, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `id_brand` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_product` varchar(50) NOT NULL,
  `deskripsi` text,
  `size` varchar(10) NOT NULL,
  `warna` varchar(20) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `hargaM` int(11) DEFAULT NULL,
  `hargaJ` int(11) NOT NULL,
  `in_date` date NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_product`,`id_brand`,`id_kategori`),
  KEY `product_ibfk_2` (`id_brand`),
  KEY `id_kategori` (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id_product`, `id_brand`, `id_kategori`, `nama_product`, `deskripsi`, `size`, `warna`, `qty`, `hargaM`, `hargaJ`, `in_date`, `image`) VALUES
(2, 5, 3, 'Millet Quick Dry T-Shirt', '- K-K : 52cm<br>\r\n- Quick Dry\r\n', 'S Euro', 'Blue', 2, 55000, 100000, '2015-01-22', 'IMGP6296.jpg'),
(3, 7, 5, 'Buffalo Base Layer Long Shirt', '- K-K : 48cm<br>\r\n- Half Zip, Quick Dry', 'S Euro', 'Pink', 2, 50000, 100000, '2015-03-03', 'IMGP6319.JPG'),
(5, 1, 2, 'TNF Dubs Insulated Snow Jacket', '- K-K : 60cm<br>\r\n- Insulated Jacket Down Fill<br>\r\n- Hangat, Tebal', 'L Euro', 'Cosmic Blue Island P', 1, 350000, 500000, '0000-00-00', 'TNF dubs-insulated-snow-jacket-cosmic-blue-island-print.jpg'),
(8, 11, 4, 'Berghaus Navigator Octane', '- K-K: 58cm<br>\r\n- Windproof, waterproof, breathable Jacket<br>\r\n- <color="red">Include Tag</color>', 'L Euro', 'Solid Blue', 1, 350000, 500000, '0000-00-00', 'berghaus_octane_20843.jpg'),
(9, 11, 10, 'Berghaus Navigator Pant', '- Lingkar pinggang : 92cm<br>\r\n- Panjang : 90cm<br>\r\n- Quick Dry, Stretch<br>', '30', 'Blue Navy', 3, 135000, 200000, '0000-00-00', 'berghaus navigator.jpg'),
(10, 1, 1, 'TNF Buckland Pant', '- Lingkar Pinggang<br>\r\n- Cotton Canvas ', '38', 'Graphite Grey', 2, 175000, 350000, '0000-00-00', 'buckland grey.jpg'),
(11, 1, 1, 'TNF Buckland Pant', '- Lingkar Pinggang<br>\r\n- Cotton Canvas', '40', 'Graphite Grey', 3, 175000, 350000, '0000-00-00', 'buckland grey.jpg'),
(12, 12, 1, 'Old Navy Cargo Canvas Short Pant', '- Lingkar Pinggang : 110 cm<br>\r\n- Bahan : Canvas Jeans <br>\r\n- Cross Label', '40 ', 'Light Brown', 1, 80000, 150000, '2016-02-02', 'IMG_7553.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `sold`
--

CREATE TABLE IF NOT EXISTS `sold` (
  `id_sold` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `no_Hp` varchar(20) DEFAULT NULL,
  `alamat` text,
  `pembayaran` int(11) DEFAULT NULL,
  `out_date` date DEFAULT NULL,
  `ongkir` int(11) DEFAULT NULL,
  `resi` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_sold`,`id_product`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_stok_opname`
--
ALTER TABLE `detail_stok_opname`
  ADD CONSTRAINT `detail_stok_opname_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`);

--
-- Constraints for table `draft`
--
ALTER TABLE `draft`
  ADD CONSTRAINT `draft_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`);

--
-- Constraints for table `header_stok_opname`
--
ALTER TABLE `header_stok_opname`
  ADD CONSTRAINT `header_stok_opname_ibfk_1` FOREIGN KEY (`id_so`) REFERENCES `detail_stok_opname` (`id_so`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`id_brand`) REFERENCES `brand` (`id_brand`),
  ADD CONSTRAINT `product_ibfk_3` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`);

--
-- Constraints for table `sold`
--
ALTER TABLE `sold`
  ADD CONSTRAINT `sold_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
