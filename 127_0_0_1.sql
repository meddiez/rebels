-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2016 at 08:06 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cdcol`
--

-- --------------------------------------------------------

--
-- Table structure for table `cds`
--

CREATE TABLE IF NOT EXISTS `cds` (
  `titel` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `interpret` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `jahr` int(11) DEFAULT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `cds`
--

INSERT INTO `cds` (`titel`, `interpret`, `jahr`, `id`) VALUES
('Beauty', 'Ryuichi Sakamoto', 1990, 1),
('Goodbye Country (Hello Nightclub)', 'Groove Armada', 2001, 4),
('Glee', 'Bran Van 3000', 1997, 5);
--
-- Database: `compro`
--

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `id_faq` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY (`id_faq`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id_faq`, `question`, `answer`) VALUES
(1, 'pertanyaan 1', 'jawaban 1'),
(2, 'pertanyaan 2', 'jawaban 2');

-- --------------------------------------------------------

--
-- Table structure for table `other`
--

CREATE TABLE IF NOT EXISTS `other` (
  `about_us` text NOT NULL,
  `contact_us` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `fitur` text NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `description`, `price`, `fitur`) VALUES
(1, 'ASUS Travel Charger 2.1A', 'ASUS Travel Charger 2.1 A with 2 USB Ports', '125001', '- Charge more than 1 device<br>- Fast Charging<br>- Light weight<br>'),
(2, 'Asus Zenfone 2', '- Snap Dragon Quad Core<br>\r\n- RAM 2GB<br>\r\n- 5" HD<br>', '4999999', 'Free 3 AON 20GB'),
(3, 'Asus Zenfone 2', '- Snap Dragon Quad Core<br>\r\n- RAM 2GB<br>\r\n- 5" HD<br>', '4999999', 'Free 3 AON 20GB'),
(4, 'Product X', 'Product X', '123456', 'blablabla'),
(5, 'Sambal ABC 125ml', 'Sambal Pedas ABC dari cabe asli', '12000', '-'),
(6, 'Penggaris Besi', 'Pajang 60cm', '20000', 'Kuat');
--
-- Database: `db_roe`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `id_brand` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(25) NOT NULL,
  PRIMARY KEY (`id_brand`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id_brand`, `brand`) VALUES
(1, 'The North Face'),
(2, 'Jack Wolfskin'),
(3, 'Columbia'),
(4, 'Trespass'),
(5, 'Millet'),
(6, 'Salomon'),
(7, 'Buffalo'),
(8, 'Karrimor'),
(9, 'Lafuma'),
(10, 'Iguana'),
(11, 'Berghaus'),
(12, 'Old Navy'),
(13, 'Rugged');

-- --------------------------------------------------------

--
-- Table structure for table `buyer`
--

CREATE TABLE IF NOT EXISTS `buyer` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pelanggan` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `no_telf` varchar(15) NOT NULL,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `detail_stok_opname`
--

CREATE TABLE IF NOT EXISTS `detail_stok_opname` (
  `id_so` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `qty_system` int(11) NOT NULL,
  `qty_check` int(11) NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`id_so`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_stok_opname`
--

INSERT INTO `detail_stok_opname` (`id_so`, `id_product`, `qty_system`, `qty_check`, `note`) VALUES
(1, 1, 10, 10, 'ok'),
(1, 2, 1, 0, 'barang rusak, dikembalikan ke supplier'),
(1, 3, 2, 2, 'ok'),
(1, 5, 1, 1, 'ok'),
(1, 8, 1, 1, 'ok'),
(1, 9, 1, 1, 'ok'),
(1, 10, 2, 2, 'ok'),
(1, 11, 3, 2, 'tidak ada kancing, barang diretur ke supplier');

-- --------------------------------------------------------

--
-- Table structure for table `header_stok_opname`
--

CREATE TABLE IF NOT EXISTS `header_stok_opname` (
  `id_so` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `so_cek` date NOT NULL,
  PRIMARY KEY (`id_so`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(25) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`) VALUES
(1, 'Celana'),
(2, 'Jacket Inner / Polar'),
(3, 'T-Shirt'),
(4, 'Jacket Waterproof'),
(5, 'Fleece / Quick Dry '),
(6, 'Celana Waterproof'),
(7, 'Kemeja'),
(8, 'Sepatu '),
(9, 'Sepatu Waterproof'),
(10, 'Carrier'),
(11, 'Matras'),
(12, 'Sleeping Bag'),
(13, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `id_brand` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_product` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `size` varchar(10) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `hargaM` int(11) NOT NULL,
  `hargaJ` int(11) NOT NULL,
  `in_date` date NOT NULL,
  `image` varchar(200) NOT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id_product`, `id_brand`, `id_kategori`, `nama_product`, `deskripsi`, `size`, `warna`, `qty`, `hargaM`, `hargaJ`, `in_date`, `image`) VALUES
(1, 2, 3, 'JWS Paw Print T-Shirt', '- K-K : 56cm<br>\r\n- P : 66cm<br>\r\n- Organic', 'L Euro', 'Fossil', 10, 60000, 120000, '2015-01-22', '181414385050.jpg'),
(2, 5, 3, 'Millet Quick Dry T-Shirt', '- K-K : 52cm<br>\r\n- Quick Dry\r\n', 'S Euro', 'Blue', 2, 55000, 100000, '2015-01-22', 'IMGP6296.jpg'),
(3, 7, 5, 'Buffalo Base Layer Long Shirt', '- K-K : 48cm<br>\r\n- Half Zip, Quick Dry', 'S Euro', 'Pink', 2, 50000, 100000, '2015-03-03', 'IMGP6319.JPG'),
(5, 1, 2, 'TNF Dubs Insulated Snow Jacked', '- K-K : 60cm<br>\r\n- Insulated Jacket Down Fill<br>\r\n- Hangat, Tebal', 'L Euro', 'Cosmic Blue Island P', 1, 350000, 500000, '0000-00-00', 'TNF dubs-insulated-snow-jacket-cosmic-blue-island-print.jpg'),
(8, 11, 4, 'Berghaus Navigator Octane', '- K-K: 58cm<br>\r\n- Windproof, waterproof, breathable Jacket<br>\r\n- <color="red">Include Tag</color>', 'L Euro', 'Solid Blue', 1, 350000, 500000, '0000-00-00', 'berghaus_octane_20843.jpg'),
(9, 11, 10, 'Berghaus Navigator Pant', '- Lingkar pinggang : 92cm<br>\r\n- Panjang : 90cm<br>\r\n- Quick Dry, Stretch<br>', '30', 'Blue Navy', 3, 135000, 200000, '0000-00-00', 'berghaus navigator.jpg'),
(10, 1, 1, 'TNF Buckland Pant', '- Lingkar Pinggang<br>\r\n- Cotton Canvas ', '38', 'Graphite Grey', 2, 175000, 350000, '0000-00-00', 'buckland grey.jpg'),
(11, 1, 1, 'TNF Buckland Pant', '- Lingkar Pinggang<br>\r\n- Cotton Canvas', '40', 'Graphite Grey', 3, 175000, 350000, '0000-00-00', 'buckland grey.jpg'),
(12, 12, 1, 'Old Navy Cargo Canvas Short Pant', '- Lingkar Pinggang : 110 cm<br>\r\n- Bahan : Canvas Jeans <br>\r\n- Cross Label', '40 ', 'Light Brown', 1, 80000, 150000, '2016-02-02', 'IMG_7553.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `sold`
--

CREATE TABLE IF NOT EXISTS `sold` (
  `id_sold` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `buyer` varchar(100) NOT NULL,
  `hargaD` int(11) NOT NULL,
  `out_date` date NOT NULL,
  `alamat` text NOT NULL,
  `ongkir` int(11) NOT NULL,
  `resi` varchar(20) NOT NULL,
  PRIMARY KEY (`id_sold`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Database: `inventori`
--
--
-- Database: `magento_1_9_1_0`
--
--
-- Database: `phpmyadmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `pma_bookmark`
--

CREATE TABLE IF NOT EXISTS `pma_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pma_column_info`
--

CREATE TABLE IF NOT EXISTS `pma_column_info` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin' AUTO_INCREMENT=56 ;

--
-- Dumping data for table `pma_column_info`
--

INSERT INTO `pma_column_info` (`id`, `db_name`, `table_name`, `column_name`, `comment`, `mimetype`, `transformation`, `transformation_options`) VALUES
(1, 'compro', 'product', 'product_id', '', '', '_', ''),
(2, 'compro', 'product', 'product_name', '', '', '_', ''),
(3, 'compro', 'product', 'description', '', '', '_', ''),
(4, 'compro', 'product', 'price', '', '', '_', ''),
(5, 'compro', 'product', 'fitur', '', '', '_', ''),
(6, 'compro', 'faq', 'id_faq', '', '', '_', ''),
(7, 'compro', 'faq', 'question', '', '', '_', ''),
(8, 'compro', 'faq', 'answer', '', '', '_', ''),
(9, 'compro', 'other', 'about_us', '', '', '_', ''),
(10, 'compro', 'other', 'contact_us', '', '', '_', ''),
(11, 'db_roe', 'product', 'id_product', '', '', '_', ''),
(12, 'db_roe', 'product', 'nama_product', '', '', '_', ''),
(13, 'db_roe', 'product', 'deskripsi', '', '', '_', ''),
(14, 'db_roe', 'product', 'size', '', '', '_', ''),
(15, 'db_roe', 'product', 'warna', '', '', '_', ''),
(16, 'db_roe', 'product', 'qty', '', '', '_', ''),
(17, 'db_roe', 'product', 'hargaM', '', '', '_', ''),
(18, 'db_roe', 'product', 'hargaJ', '', '', '_', ''),
(19, 'db_roe', 'product', 'brand', '', '', '_', ''),
(20, 'db_roe', 'product', 'kategori', '', '', '_', ''),
(21, 'db_roe', 'brand', 'id_brand', '', '', '_', ''),
(22, 'db_roe', 'brand', 'brand', '', '', '_', ''),
(23, 'db_roe', 'kategori', 'id_kategori', '', '', '_', ''),
(24, 'db_roe', 'kategori', 'kategori', '', '', '_', ''),
(25, 'db_roe', 'buyer', 'id_pelanggan', '', '', '_', ''),
(26, 'db_roe', 'buyer', 'nama_pelanggan', '', '', '_', ''),
(27, 'db_roe', 'buyer', 'alamat', '', '', '_', ''),
(28, 'db_roe', 'buyer', 'no_telf', '', '', '_', ''),
(29, 'db_roe', 'sold', 'id_product', '', '', '_', ''),
(30, 'db_roe', 'sold', 'id_buyer', '', '', '_', ''),
(31, 'db_roe', 'sold', 'hargaD', '', '', '_', ''),
(32, 'db_roe', 'sold', 'ongkir', '', '', '_', ''),
(33, 'db_roe', 'sold', 'resi', '', '', '_', ''),
(34, 'db_roe', 'product', 'id_brand', '', '', '_', ''),
(35, 'db_roe', 'product', 'id_kategori', '', '', '_', ''),
(50, 'db_roe', 'sold', 'out_date', '', '', '_', ''),
(51, 'db_roe', 'detail_stok_opname', 'note', '', '', '_', ''),
(49, 'db_roe', 'header_stok_opname', 'so_cek', '', '', '_', ''),
(41, 'db_roe', 'header_stok_opname', 'id_so', '', '', '_', ''),
(42, 'db_roe', 'header_stok_opname', 'username', '', '', '_', ''),
(43, 'db_roe', 'detail_so', 'id_so', '', '', '_', ''),
(44, 'db_roe', 'detail_so', 'id_product', '', '', '_', ''),
(45, 'db_roe', 'detail_so', 'qty_system', '', '', '_', ''),
(46, 'db_roe', 'detail_so', 'qty_check', '', '', '_', ''),
(47, 'db_roe', 'detail_so', 'comment', '', '', '_', ''),
(48, 'db_roe', 'product', 'in_date', '', '', '_', ''),
(52, 'db_roe', 'product', 'image', '', '', '_', ''),
(53, 'db_roe', 'sold', 'id_sold', '', '', '_', ''),
(54, 'db_roe', 'sold', 'buyer', '', '', '_', ''),
(55, 'db_roe', 'sold', 'alamat', '', '', '_', '');

-- --------------------------------------------------------

--
-- Table structure for table `pma_designer_coords`
--

CREATE TABLE IF NOT EXISTS `pma_designer_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `v` tinyint(4) DEFAULT NULL,
  `h` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`db_name`,`table_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma_history`
--

CREATE TABLE IF NOT EXISTS `pma_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`,`db`,`table`,`timevalue`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pma_pdf_pages`
--

CREATE TABLE IF NOT EXISTS `pma_pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`page_nr`),
  KEY `db_name` (`db_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pma_recent`
--

CREATE TABLE IF NOT EXISTS `pma_recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma_recent`
--

INSERT INTO `pma_recent` (`username`, `tables`) VALUES
('root', '[{"db":"db_roe","table":"product"},{"db":"db_roe","table":"sold"},{"db":"db_roe","table":"buyer"},{"db":"db_roe","table":"detail_stok_opname"},{"db":"db_roe","table":"header_stok_opname"},{"db":"db_roe","table":"kategori"},{"db":"db_roe","table":"brand"},{"db":"compro","table":"product"},{"db":"db_roe","table":"asset"},{"db":"db_roe","table":"detail_so"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma_relation`
--

CREATE TABLE IF NOT EXISTS `pma_relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  KEY `foreign_field` (`foreign_db`,`foreign_table`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma_table_coords`
--

CREATE TABLE IF NOT EXISTS `pma_table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float unsigned NOT NULL DEFAULT '0',
  `y` float unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma_table_info`
--

CREATE TABLE IF NOT EXISTS `pma_table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`db_name`,`table_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma_table_uiprefs`
--

CREATE TABLE IF NOT EXISTS `pma_table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`,`db_name`,`table_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

--
-- Dumping data for table `pma_table_uiprefs`
--

INSERT INTO `pma_table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'db_roe', 'brand', '{"sorted_col":"`brand`.`brand` ASC"}', '2015-11-17 07:24:22'),
('root', 'db_roe', 'product', '{"sorted_col":"`id_product` ASC"}', '2016-02-23 14:57:55'),
('root', 'db_roe', 'kategori', '{"sorted_col":"`kategori`.`kategori` ASC"}', '2015-11-17 07:30:20');

-- --------------------------------------------------------

--
-- Table structure for table `pma_tracking`
--

CREATE TABLE IF NOT EXISTS `pma_tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`db_name`,`table_name`,`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma_userconfig`
--

CREATE TABLE IF NOT EXISTS `pma_userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma_userconfig`
--

INSERT INTO `pma_userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2015-10-08 12:18:05', '{"collation_connection":"utf8mb4_general_ci"}');
--
-- Database: `test`
--
--
-- Database: `webauth`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_pwd`
--

CREATE TABLE IF NOT EXISTS `user_pwd` (
  `name` char(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `pass` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `user_pwd`
--

INSERT INTO `user_pwd` (`name`, `pass`) VALUES
('xampp', 'wampp');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
